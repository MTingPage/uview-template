/*
 * @Author: zihong
 * @Date: 2021-07-01 00:05:45
 * @LastEditTime: 2021-07-01 00:05:45
 * @LastEditors: Please set LastEditors
 * @Description: main
 * @FilePath: /uview-template/main.js
 */
import Vue from 'vue'
import App from './App'
import '@/common/request.js'
// import { http } from '@/common/request.js'

Vue.config.productionTip = false

App.mpType = 'app'

// 引入全局uView
import uView from './libs/uview-ui';
Vue.use(uView);

// 引入uView对小程序分享的mixin封装
let mpShare = require('./libs/uview-ui/libs/mixin/mpShare.js');
Vue.mixin(mpShare);

// 引入request
// Vue.prototype.$http = http

import dayjs from '@/utils/day.min.js'
Vue.prototype.dayjs = dayjs
/*
使用方法：
console.log(this.dayjs().format('YYYY-MM-DD HH:mm:ss')) // 今天
console.log(this.dayjs().subtract(1, 'day').format('YYYY-MM-DD')）// 昨天
console.log(this.dayjs().add(1,'day').format('YYYY-MM-DD HH:mm:ss')) // 明天
*/ 

const app = new Vue({
    ...App
})
app.$mount()
