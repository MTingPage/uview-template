# 常见报错与解决


1. 小程序运行时会报错：找不到`forceUpdate`，是因为uview ui 组件中使用vue语法`this.$forceUpdate();`，解决方法为申请**微信小程序AppId**重新编译即可

> `this.$forceUpdate();//数据层次太多，render函数没有自动更新页面不会重新渲染的问题，需手动强制刷新。`

涉及组件：

- u-collapse-item
- u-picker
- u-upload

2. 小程序运行时报错：`sitemap.json Error: 未找到入口 sitemap.json 文件`

- 在unpackage/dist/dev/mp-weixin 新建 sitemap.json
- 规则如下[微信小程序文档](https://developers.weixin.qq.com/miniprogram/dev/reference/configuration/sitemap.html)
- 示例：
```javasript

{
  "desc": "关于本文件的更多信息，请参考文档 https://developers.weixin.qq.com/miniprogram/dev/framework/sitemap.html",
  "rules": [{
  "action": "allow",
  "page": "*"
  }]
}

```
