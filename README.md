# uview-template

#### 介绍

最新更新见[链接](https://gitee.com/zihong019/uview-template)


#### 软件架构

软件架构说明

- api library ： [luch-request](https://www.quanzhan.co/luch-request/guide/3.x/#%E4%BB%8B%E7%BB%8D)
- animation library ： [GraceUI](https://ext.dcloud.net.cn/plugin?id=4875)
- ui component ： [uview ui](https://www.uviewui.com/)
- login : [极简登录注册模板](https://ext.dcloud.net.cn/plugin?id=538)
- app scan qrcode: [自定义扫码组件](https://ext.dcloud.net.cn/plugin?id=2759)
- dayjs 轻量的处理时间和日期的 JavaScript 库 : [dayjs 中文文档](https://day.js.org/zh-CN/)


---

- vuex 暂未使用

##  file tree

```bash

.
├── App.vue
├── LICENSE
├── README.BUG.md
├── README.md
├── api
│   ├── common.js
│   └── test.js
├── common
│   ├── config.js
│   ├── request.js
│   └── setting.js
├── components
│   └── watch-login
├── config
│   ├── env.dev.js
│   └── env.prod.js
├── libs
│   ├── GAL
│   ├── day.min.js
│   ├── luch-request
│   └── uview-ui
│      
├── main.js
├── manifest.json
├── pages
│   ├── login
│   └── tabbar
│       ├── home
│       └── user
├── pages.json
├── static
├── uni.scss
```

#### 使用说明

1.  api 接口使用说明

- 方式一：

```javascript
// 引入
import { getAllList } from "@/api/common";

const url = "https://api.thecatapi.com/v1/images/search";
getAllList(url)
  .then((result) => {
    console.log(result.data);
    // ...
  })
  .catch((err) => {
    console.log(err);
  });
```

- 方法二：

```javascript

// api/test

const apiUrl = {
	// get all detail api
	getAllDetailList:'xxxx'
}

export function getAllDetailList(params={}){
  return http.get(apiUrl.getAllDetailList,params)
}

// vue
import { getAllDetailList } from "@/api/test";

const params = {...}
getAllDetailList(params)
  .then((result) => {
    console.log(result.data);
    // ...
  })
  .catch((err) => {
    console.log(err);
  });


```

2.  ui库的使用(文档)

- animation library ： [GraceUI](https://www.graceui.com/animate/info/10185-0.html)
- ui component ： [uview ui](https://www.uviewui.com/)

3.  env环境配置

```javascript

// @/config/env.dev.js 


const baseUrl = ''

export default {
	baseUrl,
}

// common/config 


import _envConfig from '@/config/env.dev.js'
import _prodConfig from '@/config/env.prod.js'

function env_config(){
	if (process.env.NODE_ENV === "development") {
		return _envConfig
	} else if (process.env.NODE_ENV === "production") {  
		return _prodConfig
	}  
}

const config = env_config()
	
export default {
	config
}


```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 常见bug

> 见 `README.BUG.md` 文件

#### 计划

- 表格组件,是否可筛选，是否固定表头或者标尾，点击row触发事件，是否可懒加载，是否可单选多选，是否可自定义模板，是否显示合计，是否显示自定义索引，对齐方式
- 可搜索的输入框，下拉加载，是否可多选，已选未选分离，是否携带icon(靠左靠右)